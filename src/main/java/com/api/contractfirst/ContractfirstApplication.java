package com.api.contractfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContractfirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContractfirstApplication.class, args);
	}

}
